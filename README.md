# Like App Client
Like App Client is ReactJs based application which provides user interface for simple 
social interaction. Among the features available it's worth mentioning that users are able to signup, register, view other user's profile and like each other. Application internally uses *Redux* for state management and *Reactstrap* for creating user interfaces.

## Usage
To be able to run application you need to have **NodeJs v8** or later installed on your machine.

Like App Client can't function without **[Like App Server](https://gitlab.com/vejsilh/like-app-server)** application, which means that before running this project, you'll need to configure and run backend application. After setting up Like App Server, you're ready to run this project.

After cloning the project, install it's dependencies by executing:

    npm install
Now you're ready to start app in **development** mode by running:

    npm start
Open [http://localhost:3000](http://localhost:3000) to view app in the browser.

To run application in **production** mode, please refer to [React's official guide](https://facebook.github.io/create-react-app/docs/deployment).

## Running Unit Tests
All test files in the project have `*.test.js` extension. Currently there are 4 test files(`helpers.test.js`, `change-password.test.js`, `login.test.js` and `most-liked-user-item.test.js`).
To run all test simply execute following command:

    npm run test a
In this case parameter `a` denotes that you want to run all the test, even if there are no changes in test files since last commit.
