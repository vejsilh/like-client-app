import axios from 'axios';

class AuthApi {
  static login = data => {
    return axios({
      method: 'POST',
      url: '/login',
      auth: data,
    });
  };

  static signup = data => {
    return axios({
      method: 'POST',
      url: '/signup',
      data,
    });
  };

  static updatePassword = data => {
    return axios({
      method: 'PUT',
      url: '/me/update-password',
      data,
    });
  };
}

export default AuthApi;
