import axios from 'axios';

class UsersApi {
  static getMostLiked = userId => {
    return axios({
      method: 'GET',
      url: '/most-liked',
      params: {
        userId,
      },
    });
  };

  static like = userId => {
    return axios({
      method: 'PUT',
      url: `/user/${userId}/like`,
    });
  };

  static unlike = userId => {
    return axios({
      method: 'PUT',
      url: `/user/${userId}/unlike`,
    });
  };

  static getInfo = userId => {
    return axios({
      method: 'GET',
      url: `/user/${userId}`,
    });
  };

  static getCurrent = () => {
    return axios({
      method: 'GET',
      url: `/me`,
    });
  };
}

export default UsersApi;
