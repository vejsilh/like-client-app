import React, { Component } from 'react';
import { withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Container, Row, Col, Form, FormGroup, Label, Input, Button, FormFeedback } from 'reactstrap';

import { login, handleLoginFailed } from './auth-actions';
import { ROUTE_PATHS } from '../../core/routes';
import Helpers from '../../utils/helpers';

export class Login extends Component {
  state = {
    formErrors: { username: false, password: false },
    formValues: { username: '', password: '' },
    isTouched: false,
  };

  enableFormSubmit = () => {
    const { loginFailed, handleLoginFailed } = this.props;

    if (loginFailed) {
      handleLoginFailed(false);
    }
  };

  gotoSignup = () => {
    const { history } = this.props;

    history.push(ROUTE_PATHS.SIGNUP);
  };

  handleUsernameChange = event => {
    const username = event.target.value;
    const isValid = Helpers.validateUsername(username);

    this.setState({
      formValues: { ...this.state.formValues, username },
      formErrors: { ...this.state.formErrors, username: !isValid },
      isTouched: true,
    });

    this.enableFormSubmit();
  };

  handlePasswordChange = event => {
    const password = event.target.value;
    const isValid = Boolean(password);

    this.setState({
      formValues: { ...this.state.formValues, password },
      formErrors: { ...this.state.formErrors, password: !isValid },
      isTouched: true,
    });

    this.enableFormSubmit();
  };

  handleFormSubmit = event => {
    const { login } = this.props;
    const { formValues } = this.state;

    event.preventDefault();

    login(formValues);
  };

  render() {
    const { loginFailed, loggedUser } = this.props;
    const { formValues, formErrors, isTouched } = this.state;
    const isValidForm = Helpers.isValidForm(formErrors) && isTouched;
    const isAlreadyLogged = Helpers.isAuthenticated(loggedUser);

    if (isAlreadyLogged) {
      return <Redirect to={ROUTE_PATHS.ROOT} />;
    }

    return (
      <Container className="login">
        <h2>Log In</h2>
        <Form className="form" onSubmit={this.handleFormSubmit} noValidate>
          <Col>
            <FormGroup>
              <Label>Username</Label>
              <Input type="username" name="username" placeholder="Username" value={formValues.username} invalid={formErrors.username} onChange={this.handleUsernameChange} />
              <FormFeedback>Username must be between 2 and 50 characters long.</FormFeedback>
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label>Password</Label>
              <Input type="password" name="password" placeholder="********" value={formValues.password} invalid={formErrors.password} onChange={this.handlePasswordChange} />
              <FormFeedback>Password is required</FormFeedback>
            </FormGroup>
          </Col>
          <Row>
            <Col xs="3">
              <Button type="submit" color="primary" disabled={!isValidForm || loginFailed}>
                Log In
              </Button>
            </Col>
            <Col>
              <div className="float-right">
                <span>You don't have an account?</span>
                <Button type="button" color="link" onClick={this.gotoSignup}>
                  Sign Up
                </Button>
              </div>
            </Col>
          </Row>
        </Form>
      </Container>
    );
  }
}

Login.propTypes = {
  login: PropTypes.func.isRequired,
  handleLoginFailed: PropTypes.func.isRequired,
  loginFailed: PropTypes.bool.isRequired,
  loggedUser: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  loginFailed: state.auth.loginFailed,
  loggedUser: state.auth.loggedUser,
});

const mapDispatchToProps = dispatch => ({
  login: data => {
    dispatch(login(data));
  },
  handleLoginFailed: data => {
    dispatch(handleLoginFailed(data));
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Login)
);
