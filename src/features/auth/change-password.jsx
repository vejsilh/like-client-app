import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Container, Col, Form, FormGroup, Label, Input, Button, FormFeedback } from 'reactstrap';

import { changePassword, handlePasswordChangeFailed } from './auth-actions';
import { CHANGE_PASSWORD_FIELDS } from './auth-constants';
import Helpers from '../../utils/helpers';
import { ROUTE_PATHS } from '../../core/routes';

class ChangePassword extends Component {
  state = {
    formErrors: CHANGE_PASSWORD_FIELDS.reduce((errors, field) => {
      errors[field] = false;
      return errors;
    }, {}),
    formValues: CHANGE_PASSWORD_FIELDS.reduce((values, field) => {
      values[field] = '';
      return values;
    }, {}),
    isTouched: false,
  };

  enableFormSubmit = () => {
    const { passwordChangeFailed, handlePasswordChangeFailed } = this.props;

    if (passwordChangeFailed) {
      handlePasswordChangeFailed(false);
    }
  };

  gotoHome = () => {
    const { history } = this.props;

    history.push(ROUTE_PATHS.ROOT);
  };

  handlePasswordChange = event => {
    const confirmPassField = 'newPasswordConfirm';
    const field = event.target.name;
    const password = event.target.value;
    const isValid = field === confirmPassField ? true : Helpers.validatePassword(password);

    this.setState({
      formValues: { ...this.state.formValues, [field]: password },
      formErrors: { ...this.state.formErrors, [field]: !isValid, newPasswordConfirm: false },
      isTouched: true,
    });

    this.enableFormSubmit();
  };

  handleFormSubmit = event => {
    const { changePassword } = this.props;
    const { formValues } = this.state;

    event.preventDefault(formValues);

    if (formValues.newPassword !== formValues.newPasswordConfirm) {
      this.setState({
        formErrors: { ...this.state.formErrors, newPasswordConfirm: true },
      });

      return;
    }

    changePassword(formValues);
  };

  renderPasswordChangedMessage = () => (
    <p className="password-changed">
      <span>Success! Your Password has been changed! Go to</span>
      <Button color="link" onClick={this.gotoHome}>
        Home.
      </Button>
    </p>
  );

  render() {
    const { passwordChangeFailed, passwordChanged } = this.props;
    const { formValues, formErrors, isTouched } = this.state;
    const isValidForm = Helpers.isValidForm(formErrors) && isTouched;

    return (
      <Container className="change-password">
        <h2>Change your password</h2>

        {!passwordChanged ? (
          <Form className="form" onSubmit={this.handleFormSubmit} noValidate>
            <Col>
              <FormGroup>
                <Label>Current Password</Label>
                <Input type="password" name="currentPassword" value={formValues.currentPassword} invalid={formErrors.currentPassword} onChange={this.handlePasswordChange} />
                <FormFeedback>Password must be between 3 and 30 characters long and should contain letters and/or numbers.</FormFeedback>
              </FormGroup>
            </Col>
            <Col>
              <FormGroup>
                <Label>New Password</Label>
                <Input type="password" name="newPassword" value={formValues.newPassword} invalid={formErrors.newPassword} onChange={this.handlePasswordChange} />
                <FormFeedback>Password must be between 3 and 30 characters long and should contain letters and/or numbers.</FormFeedback>
              </FormGroup>
            </Col>
            <Col>
              <FormGroup>
                <Label>Confirm New Password</Label>
                <Input type="password" name="newPasswordConfirm" value={formValues.newPasswordConfirm} invalid={formErrors.newPasswordConfirm} onChange={this.handlePasswordChange} />
                <FormFeedback>Confirm password must be same as Password</FormFeedback>
              </FormGroup>
            </Col>
            <Button type="submit" color="primary" disabled={!isValidForm || passwordChangeFailed}>
              Submit
            </Button>
          </Form>
        ) : (
          this.renderPasswordChangedMessage()
        )}
      </Container>
    );
  }
}

ChangePassword.propTypes = {
  changePassword: PropTypes.func.isRequired,
  handlePasswordChangeFailed: PropTypes.func.isRequired,
  passwordChanged: PropTypes.bool.isRequired,
  passwordChangeFailed: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  passwordChanged: state.auth.passwordChanged,
  passwordChangeFailed: state.auth.passwordChangeFailed,
});

const mapDispatchToProps = dispatch => ({
  changePassword: data => {
    dispatch(changePassword(data));
  },
  handlePasswordChangeFailed: data => {
    dispatch(handlePasswordChangeFailed(data));
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ChangePassword)
);
