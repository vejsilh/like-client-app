import AuthApi from '../../api/auth-api';
import { AUTH_ACTION_TYPES, LOGIN_FAILED_ERROR, USERNAME_TAKEN_ERROR, GENERIC_SIGNUP_ERROR } from './auth-constants';
import { AUTH_TOKEN_KEY, AUTH_USER_KEY } from '../../config/constants';
import Helpers from '../../utils/helpers';

export const login = data => {
  return dispatch => {
    AuthApi.login(data).then(
      res => {
        const responseData = res.data;
        const authToken = responseData.token;
        const user = responseData.user;

        localStorage.setItem(AUTH_TOKEN_KEY, authToken);
        localStorage.setItem(AUTH_USER_KEY, JSON.stringify(user));

        dispatch(handleLoggedUserInfo(user));
      },
      () => {
        dispatch(handleLoginFailed(true));
        Helpers.showErrorMessage(LOGIN_FAILED_ERROR);
      }
    );
  };
};

export const signup = data => {
  return dispatch => {
    AuthApi.signup(data).then(
      res => {
        const responseData = res.data;
        const authToken = responseData.token;
        const user = responseData.user;

        localStorage.setItem(AUTH_TOKEN_KEY, authToken);
        localStorage.setItem(AUTH_USER_KEY, JSON.stringify(user));

        dispatch(handleLoggedUserInfo(user));
      },
      err => {
        const errMessage = err.response.status === 500 ? USERNAME_TAKEN_ERROR : GENERIC_SIGNUP_ERROR;
        dispatch(handleSignupFailed(true));
        Helpers.showErrorMessage(errMessage);
      }
    );
  };
};

export const changePassword = data => {
  return dispatch => {
    AuthApi.updatePassword(data).then(
      () => {
        dispatch(handlePasswordChanged(true));
      },
      err => {
        dispatch(handlePasswordChangeFailed(true));
        Helpers.showErrorMessage(err.response.data.error);
      }
    );
  };
};

export const logout = () => {
  return dispatch => {
    localStorage.removeItem(AUTH_TOKEN_KEY);
    localStorage.removeItem(AUTH_USER_KEY);

    dispatch(handleUserLogout());
  };
};

export const handleLoginFailed = data => ({
  type: AUTH_ACTION_TYPES.HANDLE_LOGIN_FAILED,
  data,
});

export const handleSignupFailed = data => ({
  type: AUTH_ACTION_TYPES.HANDLE_SIGNUP_FAILED,
  data,
});

export const handlePasswordChanged = data => ({
  type: AUTH_ACTION_TYPES.HANDLE_PASSWORD_CHANGED,
  data,
});

export const handlePasswordChangeFailed = data => ({
  type: AUTH_ACTION_TYPES.HANDLE_PASSWORD_CHANGE_FAILED,
  data,
});

const handleLoggedUserInfo = data => ({
  type: AUTH_ACTION_TYPES.HANDLE_LOGGED_USER_INFO,
  data,
});

const handleUserLogout = () => ({
  type: AUTH_ACTION_TYPES.HANDLE_USER_LOGOUT,
});
