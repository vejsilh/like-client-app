import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { create } from 'react-test-renderer';
import { Button } from 'reactstrap';
import configureStore from 'redux-mock-store';

import ChangePassword from './change-password';

const mockStore = configureStore();

describe('ChangePassword component', () => {
  let component = null;
  let testRenderer = null;
  let componentRoot = null;
  const initialState = {
    auth: {
      passwordChanged: false,
      passwordChangeFailed: false,
    },
  };

  beforeAll(() => {
    const store = mockStore(initialState);
    component = (
      <BrowserRouter>
        <ChangePassword store={store} changePassword={() => {}} handlePasswordChangeFailed={() => {}} />
      </BrowserRouter>
    );
    testRenderer = create(component);
    componentRoot = testRenderer.root;
  });

  it('renders three input fields', () => {
    const inputs = componentRoot.findAll(x => x.type === 'input');

    expect(inputs).toHaveLength(3);
  });

  it('renders password changed successfully message', () => {
    const newStore = mockStore({
      auth: {
        passwordChanged: true,
        passwordChangeFailed: false,
      },
    });

    testRenderer.update(
      <BrowserRouter>
        <ChangePassword store={newStore} changePassword={() => {}} handlePasswordChangeFailed={() => {}} />
      </BrowserRouter>
    );

    const messageContainer = componentRoot.findByProps({ className: 'password-changed' }).props.children;

    expect(messageContainer).toHaveLength(2);
    expect(messageContainer[0].props.children).toEqual('Success! Your Password has been changed! Go to');
    expect(messageContainer[1].type).toEqual(Button);
    expect(messageContainer[1].props.children).toEqual('Home.');
  });
});
