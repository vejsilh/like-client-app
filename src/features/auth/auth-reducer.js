import { AUTH_ACTION_TYPES } from './auth-constants';
import { AUTH_USER_KEY, AUTH_TOKEN_KEY } from '../../config/constants';

const getInitialState = () => {
  const loggedUser = (localStorage.getItem(AUTH_TOKEN_KEY) && JSON.parse(localStorage.getItem(AUTH_USER_KEY))) || {};

  return {
    loggedUser,
    loginFailed: false,
    signupFailed: false,
    passwordChanged: false,
    passwordChangeFailed: false,
  };
};

const auth = (state = getInitialState(), action) => {
  switch (action.type) {
    case AUTH_ACTION_TYPES.HANDLE_LOGGED_USER_INFO:
      return { ...state, loggedUser: action.data, loginFailed: false };
    case AUTH_ACTION_TYPES.HANDLE_LOGIN_FAILED:
      return { ...state, loginFailed: action.data };
    case AUTH_ACTION_TYPES.HANDLE_SIGNUP_FAILED:
      return { ...state, signupFailed: action.data };
    case AUTH_ACTION_TYPES.HANDLE_PASSWORD_CHANGED:
      return { ...state, passwordChanged: action.data };
    case AUTH_ACTION_TYPES.HANDLE_PASSWORD_CHANGE_FAILED:
      return { ...state, passwordChangeFailed: action.data };
    default:
      return state;
  }
};

export default auth;
