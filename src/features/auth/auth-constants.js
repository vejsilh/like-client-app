export const AUTH_ACTION_TYPES = {
  HANDLE_LOGGED_USER_INFO: 'handleLoggedUserInfo',
  HANDLE_LOGIN_FAILED: 'handleLoginFailed',
  HANDLE_SIGNUP_FAILED: 'handleSignupFailed',
  HANDLE_PASSWORD_CHANGED: 'handlePasswordChanged',
  HANDLE_PASSWORD_CHANGE_FAILED: 'handlePasswordChangeFailed',
  HANDLE_USER_LOGOUT: 'handleUserLogout',
};

export const LOGIN_FAILED_ERROR = 'Login failed. Please check your username and password';

export const USERNAME_TAKEN_ERROR = 'Username is taken';

export const GENERIC_SIGNUP_ERROR = 'Something went wrong. Please check all input fields';

export const SIGNUP_FORM_FIELDS = ['username', 'firstName', 'lastName', 'password', 'confirmPassword'];

export const CHANGE_PASSWORD_FIELDS = ['currentPassword', 'newPassword', 'newPasswordConfirm'];
