import React from 'react';
import { create } from 'react-test-renderer';

import { Login } from './login';

describe('Login component', () => {
  let component = null;
  let testRenderer = null;
  let componentRoot = null;

  beforeAll(() => {
    component = <Login login={() => {}} loginFailed={false} loggedUser={{}} handleLoginFailed={() => {}} />;
    testRenderer = create(component);
    componentRoot = testRenderer.root;
  });

  it('renders two input fields', () => {
    const inputs = componentRoot.findAll(x => x.type === 'input');

    expect(inputs).toHaveLength(2);
  });

  it('if form is untouched, Login button is disabled', () => {
    expect(componentRoot.findByProps({ color: 'primary', type: 'submit' }).props.disabled).toBe(true);
  });

  it('if to username and password which passes validation is entered, Login button is enabled', () => {
    const loginInstance = testRenderer.getInstance();

    loginInstance.handleUsernameChange({ target: { value: 'ABCDE' } });
    loginInstance.handlePasswordChange({ target: { value: '123355' } });

    testRenderer.update(<Login login={() => {}} loginFailed={false} loggedUser={{}} handleLoginFailed={() => {}} />);

    expect(componentRoot.findByProps({ color: 'primary', type: 'submit' }).props.disabled).toBe(false);
  });

  it('if login failed, Login button is disabled', () => {
    testRenderer.update(<Login login={() => {}} loginFailed={true} loggedUser={{}} handleLoginFailed={() => {}} />);

    expect(componentRoot.findByProps({ color: 'primary', type: 'submit' }).props.disabled).toBe(true);
  });

  it('if to short username is entered, Login button is disabled', () => {
    testRenderer = create(<Login login={() => {}} loginFailed={false} loggedUser={{}} handleLoginFailed={() => {}} />);
    const loginInstance = testRenderer.getInstance();

    loginInstance.handleUsernameChange({ target: { value: 'A' } });

    testRenderer.update(<Login login={() => {}} loginFailed={false} loggedUser={{}} handleLoginFailed={() => {}} />);

    expect(componentRoot.findByProps({ color: 'primary', type: 'submit' }).props.disabled).toBe(true);
  });

  it('if to long password is entered, Login button is disabled', () => {
    testRenderer = create(<Login login={() => {}} loginFailed={false} loggedUser={{}} handleLoginFailed={() => {}} />);
    const loginInstance = testRenderer.getInstance();

    loginInstance.handlePasswordChange({ target: { value: '1234567123456789101234567891089101234567891012345678910' } });

    testRenderer.update(<Login login={() => {}} loginFailed={false} loggedUser={{}} handleLoginFailed={() => {}} />);

    expect(componentRoot.findByProps({ color: 'primary', type: 'submit' }).props.disabled).toBe(true);
  });
});
