import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Container, Row, Col, Form, FormGroup, Label, Input, Button, FormFeedback } from 'reactstrap';

import { handleSignupFailed, signup } from './auth-actions';
import { SIGNUP_FORM_FIELDS } from './auth-constants';
import { ROUTE_PATHS } from '../../core/routes';
import Helpers from '../../utils/helpers';

class Signup extends Component {
  state = {
    formErrors: SIGNUP_FORM_FIELDS.reduce((errors, field) => {
      errors[field] = false;
      return errors;
    }, {}),
    formValues: SIGNUP_FORM_FIELDS.reduce((values, field) => {
      values[field] = '';
      return values;
    }, {}),
    isTouched: false,
  };

  gotoLogin = () => {
    const { history } = this.props;

    history.push(ROUTE_PATHS.LOGIN);
  };

  enableFormSubmit = () => {
    const { signupFailed, handleSignupFailed } = this.props;

    if (signupFailed) {
      handleSignupFailed(false);
    }
  };

  handleNameChange = event => {
    const field = event.target.name;
    const value = event.target.value;
    const isValid = Helpers.validateName(value);

    this.setState({
      formValues: { ...this.state.formValues, [field]: value },
      formErrors: { ...this.state.formErrors, [field]: !isValid },
      isTouched: true,
    });

    this.enableFormSubmit();
  };

  handlePasswordChange = event => {
    const confirmPassField = 'confirmPassword';
    const field = event.target.name;
    const password = event.target.value;
    const isValid = field === confirmPassField ? true : Helpers.validatePassword(password);

    this.setState({
      formValues: { ...this.state.formValues, [field]: password },
      formErrors: { ...this.state.formErrors, [field]: !isValid, confirmPassword: false },
      isTouched: true,
    });

    this.enableFormSubmit();
  };

  handleFormSubmit = event => {
    const { signup } = this.props;
    const { formValues } = this.state;

    event.preventDefault(formValues);

    if (formValues.password !== formValues.confirmPassword) {
      this.setState({
        formErrors: { ...this.state.formErrors, confirmPassword: true },
      });

      return;
    }

    signup(formValues);
  };

  render() {
    const { signupFailed, loggedUser } = this.props;
    const { formValues, formErrors, isTouched } = this.state;
    const isValidForm = Helpers.isValidForm(formErrors) && isTouched;
    const isAlreadyLogged = Helpers.isAuthenticated(loggedUser);

    if (isAlreadyLogged) {
      return <Redirect to={ROUTE_PATHS.ROOT} />;
    }

    return (
      <Container className="signup">
        <h2>Sign Up</h2>
        <Form className="form" onSubmit={this.handleFormSubmit} noValidate>
          <Col>
            <FormGroup>
              <Label>Username</Label>
              <Input type="username" name="username" placeholder="Username" value={formValues.username} invalid={formErrors.username} onChange={this.handleNameChange} />
              <FormFeedback>Username must be between 2 and 50 characters long.</FormFeedback>
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label>First Name</Label>
              <Input type="text" name="firstName" placeholder="John" value={formValues.firstName} invalid={formErrors.firstName} onChange={this.handleNameChange} />
              <FormFeedback>First name must be between 2 and 50 characters long.</FormFeedback>
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label>Last Name</Label>
              <Input type="text" name="lastName" placeholder="Doe" value={formValues.lastName} invalid={formErrors.lastName} onChange={this.handleNameChange} />
              <FormFeedback>Last name must be between 2 and 50 characters long.</FormFeedback>
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label>Password</Label>
              <Input type="password" name="password" placeholder="********" value={formValues.password} invalid={formErrors.password} onChange={this.handlePasswordChange} />
              <FormFeedback>Password must be between 3 and 30 characters long and should contain letters and/or numbers.</FormFeedback>
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label>Confirm Password</Label>
              <Input type="password" name="confirmPassword" placeholder="********" value={formValues.confirmPassword} invalid={formErrors.confirmPassword} onChange={this.handlePasswordChange} />
              <FormFeedback>Confirm password must be same as Password</FormFeedback>
            </FormGroup>
          </Col>

          <Row>
            <Col xs="3">
              <Button type="submit" color="primary" disabled={!isValidForm || signupFailed}>
                Sign Up
              </Button>
            </Col>
            <Col>
              <div className="float-right">
                <span>Already have an account?</span>
                <Button color="link" onClick={this.gotoLogin}>
                  Log In
                </Button>
              </div>
            </Col>
          </Row>
        </Form>
      </Container>
    );
  }
}

Signup.propTypes = {
  signup: PropTypes.func.isRequired,
  handleSignupFailed: PropTypes.func.isRequired,
  signupFailed: PropTypes.bool.isRequired,
  loggedUser: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  loggedUser: state.auth.loggedUser,
  signupFailed: state.auth.signupFailed,
});

const mapDispatchToProps = dispatch => ({
  signup: data => {
    dispatch(signup(data));
  },
  handleSignupFailed: data => {
    dispatch(handleSignupFailed(data));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Signup);
