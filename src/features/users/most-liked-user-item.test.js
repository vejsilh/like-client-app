import React from 'react';
import { create } from 'react-test-renderer';
import MostLikedUserItem from './most-liked-user-item';

describe('MostLikedUserItem component', () => {
  let testUser = null;
  let component = null;
  let testRenderer = null;
  let componentRoot = null;

  beforeAll(() => {
    testUser = {
      id: 1,
      firstName: 'test',
      lastName: 'test',
      username: 'test',
      likedByMe: 0,
      likedCount: 3,
    };

    component = <MostLikedUserItem user={testUser} loggedUser={{ id: 2 }} likingInProgress={false} toggleUserInfoModal={() => {}} handleLikeUserClick={() => {}} />;
    testRenderer = create(component);
    componentRoot = testRenderer.root;
  });

  it('shows correct user iformation', () => {
    const fullName = `${testUser.firstName} ${testUser.lastName} (${testUser.username})`;
    const totalLikesText = ['Total likes: ', testUser.likedCount];

    expect(componentRoot.findByProps({ color: 'link' }).props.children).toEqual(fullName);
    expect(componentRoot.findByProps({ xs: '2' }).props.children).toEqual(totalLikesText);
  });

  it('clicking on like button switch to unlike button', () => {
    const likeButton = componentRoot.findByProps({ size: 'sm', className: 'float-right' });
    const initialText = likeButton.props.children;
    const testUserUpdated = { ...testUser, likedByMe: !testUser.likedByMe };

    likeButton.props.onClick();
    testRenderer.update(<MostLikedUserItem user={testUserUpdated} loggedUser={{ id: 2 }} likingInProgress={false} toggleUserInfoModal={() => {}} handleLikeUserClick={() => {}} />);

    expect(initialText).toEqual('Like');
    expect(likeButton.props.children).toEqual('Unlike');
  });
});
