import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Button } from 'reactstrap';

const MostLikedUserItem = props => {
  const { loggedUser, user, likingInProgress, handleLikeUserClick, toggleUserInfoModal } = props;
  const fullName = `${user.firstName} ${user.lastName} (${user.username})`;

  return (
    <Row>
      <Col xs="6">
        <Button color="link" data-user-id={user.id} onClick={toggleUserInfoModal}>
          {fullName}
        </Button>
      </Col>
      <Col xs="2">Total likes: {user.likedCount}</Col>
      <Col xs="4">
        {loggedUser.id !== user.id && (
          <Button color={user.likedByMe ? 'warning' : 'success'} size="sm" className="float-right" data-user-id={user.id} disabled={likingInProgress} onClick={handleLikeUserClick}>
            {user.likedByMe ? 'Unlike' : 'Like'}
          </Button>
        )}
      </Col>
    </Row>
  );
};

MostLikedUserItem.propTypes = {
  user: PropTypes.object.isRequired,
  loggedUser: PropTypes.object.isRequired,
  likingInProgress: PropTypes.bool.isRequired,
  handleLikeUserClick: PropTypes.func.isRequired,
  toggleUserInfoModal: PropTypes.func.isRequired,
};

export default MostLikedUserItem;
