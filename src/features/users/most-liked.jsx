import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import isNil from 'lodash/isNil';
import find from 'lodash/find';
import { ListGroup } from 'reactstrap';
import posed, { PoseGroup } from 'react-pose';

import { getMostLikedUsers, unlikeUser, likeUser } from './users-actions';
import MostLikedUserItem from './most-liked-user-item';
import UserInfoModal from './modals/user-info-modal';

import './most-liked.scss';

const ListItem = posed.li({
  enter: { opacity: 1 },
  exit: { opacity: 0 },
});

class MostLiked extends Component {
  state = {
    isUserInfoModalOpen: false,
    targetUserId: null,
  };

  componentDidMount() {
    const { loggedUser, getMostLikedUsers } = this.props;

    getMostLikedUsers(loggedUser.id);
  }

  handleLikeUserClick = event => {
    const { mostLikedUsers, likeUser, unlikeUser } = this.props;
    const userId = parseInt(event.target.dataset.userId, 10);
    const targetUser = find(mostLikedUsers, { id: userId });

    if (targetUser.likedByMe) {
      unlikeUser(targetUser.id);
    } else {
      likeUser(targetUser.id);
    }
  };

  toggleUserInfoModal = event => {
    const { mostLikedUsers } = this.props;
    const { targetUserId } = this.state;
    const userId = parseInt(event.target.dataset.userId, 10);
    const targetUser = find(mostLikedUsers, { id: userId });

    this.setState(prevState => ({
      isUserInfoModalOpen: !prevState.isUserInfoModalOpen,
      targetUserId: isNil(targetUserId) ? targetUser.id : null,
    }));
  };

  render() {
    const { loggedUser, mostLikedUsers, likingInProgress } = this.props;
    const { isUserInfoModalOpen, targetUserId } = this.state;

    return (
      <ListGroup className="most-liked">
        <h3>Most Liked Users</h3>
        <PoseGroup>
          {mostLikedUsers.map(user => (
            <ListItem key={user.id}>
              <MostLikedUserItem
                key={user.id}
                user={user}
                loggedUser={loggedUser}
                likingInProgress={likingInProgress}
                handleLikeUserClick={this.handleLikeUserClick}
                toggleUserInfoModal={this.toggleUserInfoModal}
              />
            </ListItem>
          ))}
        </PoseGroup>
        <UserInfoModal isOpen={isUserInfoModalOpen} userId={targetUserId} toggleModal={this.toggleUserInfoModal} />
      </ListGroup>
    );
  }
}

MostLiked.propTypes = {
  likingInProgress: PropTypes.bool.isRequired,
  loggedUser: PropTypes.object.isRequired,
  mostLikedUsers: PropTypes.array.isRequired,
  getMostLikedUsers: PropTypes.func.isRequired,
  likeUser: PropTypes.func.isRequired,
  unlikeUser: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  likingInProgress: state.users.likingInProgress,
  mostLikedUsers: state.users.mostLikedUsers,
  loggedUser: state.auth.loggedUser,
});

const mapDispatchToProps = dispatch => ({
  getMostLikedUsers: userId => {
    dispatch(getMostLikedUsers(userId));
  },
  likeUser: userId => {
    dispatch(likeUser(userId));
  },
  unlikeUser: userId => {
    dispatch(unlikeUser(userId));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MostLiked);
