import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Row, Col } from 'reactstrap';

import { getUserInfo } from '../users-actions';

class UserInfoModal extends Component {
  componentDidUpdate(prevProps) {
    const { isOpen } = this.props;

    if (!prevProps.isOpen && isOpen) {
      this.loadUserInfo();
    }
  }

  loadUserInfo = () => {
    const { userId, getUserInfo } = this.props;

    getUserInfo(userId);
  };

  render() {
    const { isOpen, usersLikeInfo, fetchingUsersLikeInfo, toggleModal } = this.props;

    if (!isOpen || fetchingUsersLikeInfo) {
      return null;
    }

    return (
      <Modal isOpen={isOpen} toggle={toggleModal} centered>
        <ModalHeader toggle={this.toggle}>{`${usersLikeInfo.username} Info`}</ModalHeader>
        <ModalBody>
          <Row>
            <Col>Username</Col>
            <Col>{usersLikeInfo.username}</Col>
          </Row>
          <Row>
            <Col>Likes</Col>
            <Col>{usersLikeInfo.likedCount}</Col>
          </Row>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={toggleModal}>
            Close
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

UserInfoModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggleModal: PropTypes.func.isRequired,
  usersLikeInfo: PropTypes.object.isRequired,
  userId: PropTypes.number,
  fetchingUsersLikeInfo: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  usersLikeInfo: state.users.usersLikeInfo,
  fetchingUsersLikeInfo: state.users.fetchingUsersLikeInfo,
});

const mapDispatchToProps = dispatch => ({
  getUserInfo: userId => {
    dispatch(getUserInfo(userId));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserInfoModal);
