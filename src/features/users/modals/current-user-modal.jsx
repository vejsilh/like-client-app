import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Row, Col } from 'reactstrap';

import { getCurrentUserInfo, handleToggleCurrentUserModal } from '../users-actions';

class CurrentUserModal extends Component {
  componentDidUpdate(prevProps) {
    const { showCurrentUserModal } = this.props;

    if (!prevProps.showCurrentUserModal && showCurrentUserModal) {
      this.loadUserInfo();
    }
  }

  loadUserInfo = () => {
    const { getCurrentUserInfo } = this.props;

    getCurrentUserInfo();
  };

  toggleModal = () => {
    const { handleToggleCurrentUserModal } = this.props;

    handleToggleCurrentUserModal();
  };

  render() {
    const { showCurrentUserModal, currentUserInfo } = this.props;

    if (!showCurrentUserModal) {
      return null;
    }

    return (
      <Modal isOpen={showCurrentUserModal} toggle={this.toggleModal} centered>
        <ModalHeader toggle={this.toggle}>Your Profile Info</ModalHeader>
        <ModalBody>
          <Row>
            <Col>Full Name</Col>
            <Col>{`${currentUserInfo.firstName} ${currentUserInfo.lastName}`}</Col>
          </Row>
          <Row>
            <Col>Username</Col>
            <Col>{currentUserInfo.username}</Col>
          </Row>
          <Row>
            <Col>Likes</Col>
            <Col>{currentUserInfo.likedCount}</Col>
          </Row>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={this.toggleModal}>
            Close
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

CurrentUserModal.propTypes = {
  showCurrentUserModal: PropTypes.bool.isRequired,
  handleToggleCurrentUserModal: PropTypes.func.isRequired,
  currentUserInfo: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  currentUserInfo: state.users.currentUserInfo,
  showCurrentUserModal: state.users.showCurrentUserModal,
});

const mapDispatchToProps = dispatch => ({
  getCurrentUserInfo: () => {
    dispatch(getCurrentUserInfo());
  },
  handleToggleCurrentUserModal: () => {
    dispatch(handleToggleCurrentUserModal());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CurrentUserModal);
