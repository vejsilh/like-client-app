export const USERS_ACTION_TYPES = {
  HANDLE_MOST_LIKED_USERS: 'handleMostLikedUsers',
  HANDLE_LIKE_USER: 'handleLikeUser',
  HANDLE_UNLIKE_USER: 'handleUnlikeUser',
  HANDLE_LIKING_IN_PROGRESS: 'handleLikingInProgress',
  HANDLE_USERS_LIKES_INFO: 'handleUserLikesInfo',
  HANDLE_FETCHING_USERS_LIKES_INFO: 'handleFetchingUserLikesInfo',
  HANDLE_CURRENT_USER_INFO: 'handleCurrentUserInfo',
  HANDLE_TOGGLE_CURRENT_USER_MODAL: 'handleToggleCurrentUserModal',
};
