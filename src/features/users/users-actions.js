import { USERS_ACTION_TYPES } from './users-constants';
import UsersApi from '../../api/users-api';
import Helpers from '../../utils/helpers';

export const getMostLikedUsers = userId => {
  return dispatch => {
    UsersApi.getMostLiked(userId).then(res => {
      dispatch(handleMostLikedUsers(res.data));
    });
  };
};

export const likeUser = userId => {
  return dispatch => {
    dispatch(handleLikingInProgress(true));
    UsersApi.like(userId).then(
      () => {
        dispatch(handleLikingInProgress(false));
        dispatch(handleLikeUser(userId));
      },
      err => {
        dispatch(handleLikingInProgress(false));
        Helpers.showErrorMessage(err.response.data.error);
      }
    );
  };
};

export const unlikeUser = userId => {
  return dispatch => {
    dispatch(handleLikingInProgress(true));
    UsersApi.unlike(userId).then(
      () => {
        dispatch(handleLikingInProgress(false));
        dispatch(handleUnlikeUser(userId));
      },
      err => {
        dispatch(handleLikingInProgress(false));
        Helpers.showErrorMessage(err.response.data.error);
      }
    );
  };
};

export const getUserInfo = userId => {
  return dispatch => {
    dispatch(handleFetchingUsersLikeInfo(true));
    UsersApi.getInfo(userId).then(
      res => {
        dispatch(handleUsersLikeInfo(res.data));
        dispatch(handleFetchingUsersLikeInfo(false));
      },
      err => {
        dispatch(handleFetchingUsersLikeInfo(false));
        Helpers.showErrorMessage(err.response.data.error);
      }
    );
  };
};

export const getCurrentUserInfo = () => {
  return dispatch => {
    UsersApi.getCurrent().then(
      res => {
        dispatch(handleCurrentUserInfo(res.data));
      },
      err => {
        Helpers.showErrorMessage(err.response.data.error);
      }
    );
  };
};

export const handleToggleCurrentUserModal = () => ({
  type: USERS_ACTION_TYPES.HANDLE_TOGGLE_CURRENT_USER_MODAL,
});

const handleCurrentUserInfo = data => ({
  type: USERS_ACTION_TYPES.HANDLE_CURRENT_USER_INFO,
  data,
});

const handleFetchingUsersLikeInfo = data => ({
  type: USERS_ACTION_TYPES.HANDLE_FETCHING_USERS_LIKES_INFO,
  data,
});

const handleUsersLikeInfo = data => ({
  type: USERS_ACTION_TYPES.HANDLE_USERS_LIKES_INFO,
  data,
});

const handleLikingInProgress = data => ({
  type: USERS_ACTION_TYPES.HANDLE_LIKING_IN_PROGRESS,
  data,
});

const handleLikeUser = userId => ({
  type: USERS_ACTION_TYPES.HANDLE_LIKE_USER,
  userId,
});

const handleUnlikeUser = userId => ({
  type: USERS_ACTION_TYPES.HANDLE_UNLIKE_USER,
  userId,
});

const handleMostLikedUsers = data => ({
  type: USERS_ACTION_TYPES.HANDLE_MOST_LIKED_USERS,
  data,
});
