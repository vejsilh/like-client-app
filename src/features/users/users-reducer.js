import findIndex from 'lodash/findIndex';
import orderBy from 'lodash/orderBy';

import { USERS_ACTION_TYPES } from './users-constants';

const usersInitialState = {
  mostLikedUsers: [],
  likingInProgress: false,
  usersLikeInfo: {},
  fetchingUsersLikeInfo: false,
  currentUserInfo: {},
  showCurrentUserModal: false,
};

const auth = (state = usersInitialState, action) => {
  switch (action.type) {
    case USERS_ACTION_TYPES.HANDLE_MOST_LIKED_USERS:
      return { ...state, mostLikedUsers: action.data };
    case USERS_ACTION_TYPES.HANDLE_LIKING_IN_PROGRESS:
      return { ...state, likingInProgress: action.data };
    case USERS_ACTION_TYPES.HANDLE_USERS_LIKES_INFO:
      return { ...state, usersLikeInfo: action.data };
    case USERS_ACTION_TYPES.HANDLE_FETCHING_USERS_LIKES_INFO:
      return { ...state, fetchingUsersLikeInfo: action.data };
    case USERS_ACTION_TYPES.HANDLE_CURRENT_USER_INFO:
      return { ...state, currentUserInfo: action.data };
    case USERS_ACTION_TYPES.HANDLE_TOGGLE_CURRENT_USER_MODAL:
      return { ...state, showCurrentUserModal: !state.showCurrentUserModal };
    case USERS_ACTION_TYPES.HANDLE_LIKE_USER: {
      const itemIndex = findIndex(state.mostLikedUsers, { id: action.userId });
      const user = state.mostLikedUsers[itemIndex];
      state.mostLikedUsers[itemIndex] = { ...user, likedByMe: true, likedCount: user.likedCount + 1 };
      const updatedUsers = orderBy(state.mostLikedUsers, ['likedCount'], ['desc']);
      return { ...state, mostLikedUsers: updatedUsers };
    }
    case USERS_ACTION_TYPES.HANDLE_UNLIKE_USER: {
      const itemIndex = findIndex(state.mostLikedUsers, { id: action.userId });
      const user = state.mostLikedUsers[itemIndex];
      state.mostLikedUsers[itemIndex] = { ...user, likedByMe: false, likedCount: user.likedCount - 1 };
      const updatedUsers = orderBy(state.mostLikedUsers, ['likedCount'], ['desc']);
      return { ...state, mostLikedUsers: updatedUsers };
    }
    default:
      return state;
  }
};

export default auth;
