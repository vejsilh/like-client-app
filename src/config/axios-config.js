import axios from 'axios';

import { API_URL, AUTH_TOKEN_KEY, AUTH_TOKEN_PREFIX } from './constants';

const configureAxiosClient = () => {
  axios.defaults.baseURL = API_URL;

  axios.interceptors.request.use(config => {
    const authToken = localStorage.getItem(AUTH_TOKEN_KEY);

    config.headers.Authorization = `${AUTH_TOKEN_PREFIX} ${authToken}`;

    return config;
  });
};

export default configureAxiosClient;
