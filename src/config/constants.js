export const API_URL = 'http://localhost:5000';

export const USERNAME_VALIDATION = {
  MIN_LENGTH: 2,
  MAX_LENGTH: 50,
};

export const NAME_VALIDATION = {
  MIN_LENGTH: 2,
  MAX_LENGTH: 50,
};

export const PASSWORD_REGEX = /^[a-zA-Z0-9]{3,30}$/;

export const AUTH_TOKEN_KEY = 'authToken';

export const AUTH_TOKEN_PREFIX = 'Bearer';

export const AUTH_USER_KEY = 'user';
