import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import App from './core/App';
import * as serviceWorker from './serviceWorker';
import store from './core/store';
import configureAxiosClient from './config/axios-config';

configureAxiosClient();

const storeInstance = store();

ReactDOM.render(
  <Provider store={storeInstance}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);

serviceWorker.unregister();
