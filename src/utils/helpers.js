import { toast } from 'react-toastify';
import isEmpty from 'lodash/isEmpty';

import { USERNAME_VALIDATION, NAME_VALIDATION, PASSWORD_REGEX } from '../config/constants';

class Helpers {
  static validateUsername(username) {
    return Boolean(username && username.length >= USERNAME_VALIDATION.MIN_LENGTH && username.length <= USERNAME_VALIDATION.MAX_LENGTH);
  }

  static validateName(name) {
    return Boolean(name && name.length >= NAME_VALIDATION.MIN_LENGTH && name.length <= NAME_VALIDATION.MAX_LENGTH);
  }

  static validatePassword(password) {
    if (!password) {
      return false;
    }

    const regex = PASSWORD_REGEX;

    return regex.test(password);
  }

  static isValidForm(formErrors) {
    for (const key of Object.keys(formErrors)) {
      if (formErrors[key]) {
        return false;
      }
    }

    return true;
  }

  static isAuthenticated(user) {
    return !isEmpty(user);
  }

  static showErrorMessage(message) {
    toast.error(message, { autoClose: false });
  }
}

export default Helpers;
