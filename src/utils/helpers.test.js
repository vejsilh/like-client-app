import Helpers from './helpers';

describe('Helpers class', () => {
  describe('validateUsername method', () => {
    it('returns false for empty username ', () => {
      const username = '';
      const isValid = Helpers.validateUsername(username);

      expect(isValid).toBe(false);
    });

    it('returns true for 6 characters long username', () => {
      const username = '1:d!22';
      const isValid = Helpers.validateUsername(username);

      expect(isValid).toBe(true);
    });
  });

  describe('validateName method', () => {
    it('returns false for null username', () => {
      const username = null;
      const isValid = Helpers.validateName(username);

      expect(isValid).toBe(false);
    });

    it('returns true for 15 characters long name', () => {
      const username = '123456asdfghjk1';
      const isValid = Helpers.validateName(username);

      expect(isValid).toBe(true);
    });
  });

  describe('validatePassword method', () => {
    it('returns false for password which contain special characters', () => {
      const password = '1$":_"';
      const isValid = Helpers.validatePassword(password);

      expect(isValid).toBe(false);
    });

    it('returns false for null password', () => {
      const password = null;
      const isValid = Helpers.validatePassword(password);

      expect(isValid).toBe(false);
    });

    it('returns true for alphanumeric, 5 characters long password', () => {
      const password = 'mypa5';
      const isValid = Helpers.validatePassword(password);

      expect(isValid).toBe(true);
    });
  });

  describe('isValidForm method', () => {
    it('returns false for form containing error', () => {
      const formErrors = {
        username: false,
        password: true,
      };
      const isValid = Helpers.isValidForm(formErrors);

      expect(isValid).toBe(false);
    });

    it('returns true for form free of errors', () => {
      const formErrors = {
        username: false,
        password: false,
        confirmPassword: false,
      };
      const isValid = Helpers.isValidForm(formErrors);

      expect(isValid).toBe(true);
    });
  });

  describe('isAuthenticated method', () => {
    it('returns true for user object which contain some values', () => {
      const user = {
        username: 'demo',
        id: 1,
      };
      const isAuthenticated = Helpers.isAuthenticated(user);

      expect(isAuthenticated).toBe(true);
    });

    it('returns false for user object which contain no key', () => {
      const user = {};
      const isAuthenticated = Helpers.isAuthenticated(user);

      expect(isAuthenticated).toBe(false);
    });
  });
});
