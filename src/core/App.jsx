import React, { Component } from 'react';
import { Switch } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';

import routes from './routes';
import AppNavbar from './AppNavbar';
import CurrentUserModal from '../features/users/modals/current-user-modal';
import RouteWithSubRoutes from './route-with-subroutes';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';
import './App.scss';

class App extends Component {
  renderRoutes = () => {
    return routes.map((route, i) => <RouteWithSubRoutes key={i} {...route} />);
  };

  render() {
    return (
      <div className="app">
        <AppNavbar />
        <div className="app-body">
          <Switch>{this.renderRoutes()}</Switch>
        </div>
        <CurrentUserModal />
        <ToastContainer />
      </div>
    );
  }
}

export default App;
