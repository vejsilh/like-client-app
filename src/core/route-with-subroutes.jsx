import React from 'react';
import { Route } from 'react-router-dom';
import PropTypes from 'prop-types';

import PrivateRoute from './private-route';

const RouteWithSubRoutes = route => {
  if (route.requiresAuth) {
    return <PrivateRoute {...route} />;
  } else {
    return <Route path={route.path} render={props => <route.component {...props} routes={route.routes} />} />;
  }
};

RouteWithSubRoutes.propTypes = {
  path: PropTypes.string.isRequired,
  component: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
  requiresAuth: PropTypes.bool,
  routes: PropTypes.array,
};

export default RouteWithSubRoutes;
