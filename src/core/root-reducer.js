import { combineReducers } from 'redux';

import { AUTH_ACTION_TYPES } from '../features/auth/auth-constants';
import auth from '../features/auth/auth-reducer';
import users from '../features/users/users-reducer';

const appReducer = combineReducers({
  auth,
  users,
});

const rootReducer = (state, action) => {
  if (action.type === AUTH_ACTION_TYPES.HANDLE_USER_LOGOUT) {
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;
