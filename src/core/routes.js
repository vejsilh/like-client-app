import Login from '../features/auth/login';
import Signup from '../features/auth/signup';
import ChangePassword from '../features/auth/change-password';
import MostLiked from '../features/users/most-liked';

export const ROUTE_PATHS = {
  ROOT: '/',
  LOGIN: '/login',
  SIGNUP: '/signup',
  CHANGE_PASSWORD: '/change-password',
};

const routes = [
  {
    path: ROUTE_PATHS.ROOT,
    component: MostLiked,
    requiresAuth: true,
    exact: true,
  },
  {
    path: ROUTE_PATHS.LOGIN,
    component: Login,
    exact: true,
  },
  {
    path: ROUTE_PATHS.SIGNUP,
    component: Signup,
    exact: true,
  },
  {
    path: ROUTE_PATHS.CHANGE_PASSWORD,
    component: ChangePassword,
    exact: true,
    requiresAuth: true,
  },
];

export default routes;
