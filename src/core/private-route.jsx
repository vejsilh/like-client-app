import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { ROUTE_PATHS } from './routes';
import Helpers from '../utils/helpers';

const PrivateRoute = ({ component: Component, loggedUser, ...rest }) => {
  const isAuthenticated = Helpers.isAuthenticated(loggedUser);

  return <Route {...rest} render={props => (isAuthenticated ? <Component {...props} /> : <Redirect to={{ pathname: ROUTE_PATHS.LOGIN, state: { from: props.location } }} />)} />;
};

const mapStateToProps = state => ({
  loggedUser: state.auth.loggedUser,
});

export default connect(
  mapStateToProps,
  null
)(PrivateRoute);
