import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Navbar, NavbarBrand, UncontrolledDropdown, Collapse, Nav, DropdownMenu, DropdownItem, DropdownToggle } from 'reactstrap';

import { handleToggleCurrentUserModal } from '../features/users/users-actions';
import { logout } from '../features/auth/auth-actions';
import { ROUTE_PATHS } from './routes';
import Helpers from '../utils/helpers';

class AppNavbar extends Component {
  gotoChangePassword = () => {
    const { history } = this.props;

    history.push(ROUTE_PATHS.CHANGE_PASSWORD);
  };

  toggleAboutMeModal = () => {
    const { handleToggleCurrentUserModal } = this.props;

    handleToggleCurrentUserModal();
  };

  logout = () => {
    const { logout } = this.props;

    logout();
  };

  render() {
    const { loggedUser } = this.props;
    const isAuthenticated = Helpers.isAuthenticated(loggedUser);

    return (
      <Navbar color="dark" dark expand="md">
        <NavbarBrand href="/">Like App</NavbarBrand>
        {isAuthenticated && (
          <Collapse navbar>
            <Nav className="ml-auto" navbar>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  Options
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem onClick={this.toggleAboutMeModal}>About me</DropdownItem>
                  <DropdownItem onClick={this.gotoChangePassword}>Change password</DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem onClick={this.logout}>Logout</DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        )}
      </Navbar>
    );
  }
}

AppNavbar.propTypes = {
  handleToggleCurrentUserModal: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  loggedUser: state.auth.loggedUser,
});

const mapDispatchToProps = dispatch => ({
  handleToggleCurrentUserModal: () => {
    dispatch(handleToggleCurrentUserModal());
  },
  logout: () => {
    dispatch(logout());
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AppNavbar)
);
